#########################################################
# This file has been automatically generated by gem2tgz #
#########################################################
# -*- encoding: utf-8 -*-
# stub: versionist 2.0.1 ruby lib

Gem::Specification.new do |s|
  s.name = "versionist".freeze
  s.version = "2.0.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 1.3.6".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Brian Ploetz".freeze]
  s.date = "2019-07-22"
  s.description = "A plugin for versioning Rails based RESTful APIs.".freeze
  s.files = ["lib/generators/versionist/copy_api_version/USAGE".freeze, "lib/generators/versionist/copy_api_version/copy_api_version_generator.rb".freeze, "lib/generators/versionist/inflector_fixes.rb".freeze, "lib/generators/versionist/new_api_version/USAGE".freeze, "lib/generators/versionist/new_api_version/new_api_version_generator.rb".freeze, "lib/generators/versionist/new_api_version/templates/base_controller.rb".freeze, "lib/generators/versionist/new_api_version/templates/base_controller_functional_test.rb".freeze, "lib/generators/versionist/new_api_version/templates/base_controller_functional_test_rails_5.rb".freeze, "lib/generators/versionist/new_api_version/templates/base_controller_integration_test.rb".freeze, "lib/generators/versionist/new_api_version/templates/base_controller_spec.rb".freeze, "lib/generators/versionist/new_api_version/templates/base_presenter.rb".freeze, "lib/generators/versionist/new_api_version/templates/base_presenter_spec.rb".freeze, "lib/generators/versionist/new_api_version/templates/base_presenter_test.rb".freeze, "lib/generators/versionist/new_api_version/templates/base_presenter_test_rails_5.rb".freeze, "lib/generators/versionist/new_api_version/templates/docs_index.rb".freeze, "lib/generators/versionist/new_api_version/templates/docs_style.rb".freeze, "lib/generators/versionist/new_controller/USAGE".freeze, "lib/generators/versionist/new_controller/new_controller_generator.rb".freeze, "lib/generators/versionist/new_controller/templates/new_controller.rb".freeze, "lib/generators/versionist/new_controller/templates/new_controller_functional_test.rb".freeze, "lib/generators/versionist/new_controller/templates/new_controller_functional_test_rails_5.rb".freeze, "lib/generators/versionist/new_controller/templates/new_controller_integration_test.rb".freeze, "lib/generators/versionist/new_controller/templates/new_controller_spec.rb".freeze, "lib/generators/versionist/new_presenter/USAGE".freeze, "lib/generators/versionist/new_presenter/new_presenter_generator.rb".freeze, "lib/generators/versionist/new_presenter/templates/new_presenter.rb".freeze, "lib/generators/versionist/new_presenter/templates/new_presenter_spec.rb".freeze, "lib/generators/versionist/new_presenter/templates/new_presenter_test.rb".freeze, "lib/generators/versionist/new_presenter/templates/new_presenter_test_rails_5.rb".freeze, "lib/generators/versionist/rspec_helper.rb".freeze, "lib/versionist.rb".freeze, "lib/versionist/configuration.rb".freeze, "lib/versionist/middleware.rb".freeze, "lib/versionist/railtie.rb".freeze, "lib/versionist/routing.rb".freeze, "lib/versionist/version.rb".freeze, "lib/versionist/versioning_strategy.rb".freeze, "lib/versionist/versioning_strategy/base.rb".freeze, "lib/versionist/versioning_strategy/default.rb".freeze, "lib/versionist/versioning_strategy/header.rb".freeze, "lib/versionist/versioning_strategy/parameter.rb".freeze, "lib/versionist/versioning_strategy/path.rb".freeze]
  s.homepage = "https://github.com/bploetz/versionist".freeze
  s.licenses = ["MIT".freeze]
  s.rdoc_options = ["--charset=UTF-8".freeze]
  s.rubygems_version = "2.5.2.1".freeze
  s.summary = "versionist-2.0.1".freeze

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<activesupport>.freeze, [">= 3"])
      s.add_runtime_dependency(%q<railties>.freeze, [">= 3"])
      s.add_runtime_dependency(%q<yard>.freeze, ["~> 0.9.20"])
    else
      s.add_dependency(%q<activesupport>.freeze, [">= 3"])
      s.add_dependency(%q<railties>.freeze, [">= 3"])
      s.add_dependency(%q<yard>.freeze, ["~> 0.9.20"])
    end
  else
    s.add_dependency(%q<activesupport>.freeze, [">= 3"])
    s.add_dependency(%q<railties>.freeze, [">= 3"])
    s.add_dependency(%q<yard>.freeze, ["~> 0.9.20"])
  end
end
